FROM node:latest as node
WORKDIR /app
COPY . .
RUN npm install
RUN npm install -g @angular/cli
RUN npm install ngx-bootstrap --save
RUN npm add @ng-bootstrap/ng-bootstrap@3
RUN npm add ngx-bootstrap  --component carousel
RUN $(npm bin)/ng build

# stage 2
FROM nginx:alpine
COPY --from=node /app/dist/practice /usr/share/nginx/html
